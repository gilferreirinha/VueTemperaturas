
window.Event = new Vue();

// component regions
const Regions = {
  template: `
    <ul class="list-group temp-list mb-2">
        <region v-for="region in searchBy" :key="region.id" :region="region"></region>                    
    </ul>
  `,
  props: ['searchBy'],
};

// component region
const Region = {
    template: `
        <li v-on:click.prevent="toggleModal(region)" class="list-group-item d-flex align-items-center">                    
            <p class="city-name" v-text="region.name"></p>
            <img class="img" v-bind:src="'http://openweathermap.org/img/w/' + region.weather[0].icon + '.png'">
            <p class="degree-box" v-text="region.main.temp">˚</p>
        </li>     
    `,
    props: ['region'],
    methods: {
      toggleModal(region) {
        Event.$emit('toggleModal', region);
      }
    }
  };

// register modal component
const Modal = {
    template: `
      <div class="modal">
        <div class="modal-background"></div>
          <div class="modal-content">
            <div class="box">
              <div class="content">               
                <p class="city-name" v-text="modal.name"></p>
                <img class="img" v-bind:src="'http://openweathermap.org/img/w/' + modal.weather[0].icon + '.png'">
                <p class="degree-box" v-text="modal.main.temp">˚</p>
              </div>
            </div>
          </div>
          <button v-on:click="close" class="modal-close is-large" aria-label="close"></button>
      </div>
    `,
    data() {
      return {
        active: false,
        modal: false,
      }
    },
    methods: {
      show() {
        this.active = true;
      },
      close()
      {
        this.active = false;
      }
    },
    created() {
      Event.$on('toggleModal', (region) => {
        this.modal = region;
        this.show();
      });
    }
};

// register component
Vue.component('regions', Regions);
Vue.component('region', Region);
Vue.component('region-modal', Modal);

var app = new Vue({
    el: '#appTempo', 
    data: {      
        isActive: null,  
        itemSelected: null,
        region: [],
        regionList : {            
            PT : ['Porto','Lisbon','Faro','Beja','Coimbra', 'Santo Tirso', 'Vila nova de Famalicão', 'Trofa', 'Barcelos', 'Braga', 'Viana do Castelo', 'Póvoa de Varzim'],
            ES : ['Valladolid','Madrid','Barcelona','Vigo'],
            GB : ['Derry','London', 'Dublin', ],
            FR : ['Paris', 'Lile', 'Marseille', 'Lyon', 'Toulouse', 'Nantes', 'Strasbourg']
        },
        regionSmallToShow: '',
        filterCountry: '',
        searchCountry:'',        
    },

    created: function (){         
        this.getAllRegistons();         
    },

    mounted: function () {
        this.getAllKeys(this.regionList);                
    },

    computed: {

        // filter by country to show and search by city to show 
        // bease in the country or in the general
        searchBy: function() {     
            
            // clone array region
            var arrayTemp = this.region;
            
            // verify if has filter country seleted
            if (this.filterCountry != '' )
            {
                arrayTemp = arrayTemp.filter(item => {                   
                    return item.sys.country == this.filterCountry;
                });                
            }

            // verify if has search country wrotten
            if (this.searchCountry != '' )
            {
                arrayTemp = arrayTemp.filter(item => {
                    return item.name.toLowerCase().includes(this.searchCountry.toLowerCase());
                });
            }
            
            // return final array
            return arrayTemp;              
        },

    },
    
    methods: { 

        // get data regions call api
        getDataRegion: function (pLocation, pSmallName){

            var regionTemp = { name: '', main:{temp:''}, weather:[{icon:null}]}

            var stringQuery = "https://api.openweathermap.org/data/2.5/weather?q=" + pLocation + "," + pSmallName + "&APPID=71e3190eb40e16a7c2fe259e0bd5f51b";
            console.log(stringQuery);
            axios
            .get(stringQuery)
            .then(response => (regionTemp = response.data,
                    regionTemp.main.temp = this.kelvTocel(response.data.main.temp), 
                    this.region.push(regionTemp)))                       
        },       
        
        // get all registons
        getAllRegistons: function()
        {
            Object.keys(this.regionList).forEach(item => {
                this.regionList[item].forEach(itemIndex => {
                    this.getDataRegion(itemIndex, item);
                });
            });
        },

        // get all keys
        getAllKeys: function (pListMain )
        {
            this.regionSmallToShow = Object.keys(pListMain) ;
        },                
        
        // convert far to cels
        farToCel: function (pDegree)
        {  
            return  (pDegree - 32) * (5/9);
        },
        // convert degree kelv to celcs
        kelvTocel: function(pDegree)
        {
            return  Math.round((pDegree- 273.15));
        },

        // remove an item from array region
        delteItem: function(pItem)
        {                    
            this.region.splice( this.region.indexOf(pItem), 1);
        }
    }
  });